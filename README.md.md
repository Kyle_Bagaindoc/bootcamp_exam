# Time Tracking App

# Versions

version: 1.0.0+1

## Packages

- [**sdk: ">=2.12.0 <3.0.0"**](https://flutter.dev/docs/development/tools/sdk)

      Software Development Kit or SDK it has the packages and command-line tools that you need to develop Flutter apps across platforms. To get the Flutter SDK

- **cupertino_icons: ^1.0.2**

      This is an asset repo containing the default set of icon assets used by Flutter's Cupertino widgets.

- [**google_fonts: ^2.1.0**](https://pub.dev/packages/google_fonts)

      The google_fonts package for Flutter allows you to easily use any of the 977 fonts (and their variants).

- **sdk: flutter**

       The Flutter SDK has the packages and command-line tools that you need to develop Flutter apps across platforms.

- [***font_awesome_flutter: ^9.1.0***](https://pub.dev/packages/font_awesome_flutter)

       The Font Awesome Icon pack available as set of Flutter Icons.

- [**uses-material-design: true**](https://material.io/develop/flutter)

       Build beautiful, usable products using Material Components for Flutter, a mobile UI framework

- assets:

      [- assets](https://flutter.dev/docs/development/ui/assets-and-images)

      An asset is a file that is bundled and deployed with your app, and is accessible at runtime.

# Time Tracking App

- Generate Monthly/Weekly Repor
- View my account
- View business card
- View Supervisor business card
- Apply Vacation/Special/Sick Leave/s
- Edit Leave/s
- Cancel Leave/s
- Add time slots
    - Without working time
    - With working time
- Add pause time
    - Waiting time
    - Standby time
- Edit working time
- View Calendar
- Save working time
- View approved working times

# Setup Guide

 

- Cloning the project locally:
    1. 
    2. Open Terminal in your computer.
    3.  Change the current working directory to the location here you want the clone directory.
    4. Type `git clone` ,and then paste the url of your bitbucket . `git clone <url>`.
    5.  Go to your flutter project by using the command: `cd <your flutter_app_name>`
    6. Type  `flutter run` to start running your cloned flutter app.

    Another option would be: 

    1. Open VScode and take a new window.
    2. Press " ctrl+shift+p " for making command pallet to display.
    3. Type in Git in the pallet on top. 
    4. Select the suggested Git: clone option.
    5. Paste the Git URL in the pallet of the project you have cloned.

- Installing the flutter packages
    1. Depend on it

        Open the `pubspec.yaml` file located inside the app folder and add the packages you need.

    2. Install it
        - From the terminal: Run `flutter pub get`.
        - From VS Code: Click **Get Packages** located in right side of the action ribbon at the top of `pubspec.yaml`.

# Images of the App

## Sign-In Page

![signin.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/signin.jpg)

## Homepage

![homepage.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/homepage.jpg)

![homepage2.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/homepage2.jpg)

## Drawer

![drawer.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/drawer.jpg)

## Meine Visitenkarte

![business card.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/business_card.jpg)

![business card2.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/business_card2.jpg)

## Vorgesetzte

![business card3.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/business_card3.jpg)

![business card4.JPG](Time%20Tracking%20App%203a44232259704af59f66019607e18104/business_card4.jpg)