import 'package:flutter/material.dart';
import 'package:flutter_bootcamp_eaxam/screens/signin_screen.dart';

import '/screens/homepage_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
      routes: {
        '/': (context) => SigninScreen(),
        '/homepage': (context) => Homepage(),
      },
    );
  }
}