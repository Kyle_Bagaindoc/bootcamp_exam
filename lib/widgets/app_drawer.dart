import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';


import '/screens/business_card.dart';
import '/screens/homepage_screen.dart';


class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
       
       return Drawer(
           
           child: ListView(
               
              padding: EdgeInsets.zero,
              
               children: [
                  Material(
            child: Padding(
                
                padding: EdgeInsets.all( 20),
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: [ IconButton( icon: FaIcon(FontAwesomeIcons.times),
            iconSize: 20,
            hoverColor: Colors.transparent,
        onPressed: (){
        Navigator.pop(context);
        },
             ),
             
             Image.asset('assets/flutterlogo.jpg',height: 30,
            width: 30,),
            
           ],
                 ),
                  ),
                  ),
                 
                   ListTile(    
                        leading: Image.asset('assets/user contact.jpg' ),
                        title: Text('Mein Konto'), 
                        onTap: (){
                           Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => Homepage()),
                            );
                        },
                    ),
                    ListTile(
                        leading: Image.asset('assets/person_id.jpg' ),
                        title: Text('Visitenkarte'),
                        onTap: (){
                           Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => BusinessCard()),
                        );
                       },
                   ),
                   
                   ListTile(
                        leading: Image.asset('assets/time_tracking.jpg' ),
                        title: Text('Zeiterfassung '),
                        onTap: (){
                           Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Homepage()),
                        );
                       },
                   ),
                   ListTile(
                        leading: Image.asset('assets/atom.png' ),
                        title: Text('Meine Einsätze'),
                        onTap: (){
                           Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Homepage()),
                        );
                       },
                   ),
               ]
           ),
       );
    }
       
}