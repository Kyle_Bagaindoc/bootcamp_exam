import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:flutter_bootcamp_eaxam/widgets/app_drawer.dart';

class BusinessCard extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp (
            debugShowCheckedModeBanner: false,
            home: Scaffold(
                appBar: AppBar (
                    iconTheme: IconThemeData(color: Colors.black),
                    backgroundColor: Colors.white,
            
                ),
                body: Container(
                    width: double.infinity,
                    child: Business() , 
                ),
                drawer: AppDrawer(),
                backgroundColor: Colors.grey[300],  
             )
         );
    }
}

class Business extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return DefaultTabController(
            length: 2,
            child: Column(
                children: <Widget>[
                                 Material(
                            child: TabBar(
                            indicatorColor: Colors.purple,
                            indicatorWeight: 5,
                            tabs: [
                                Tab (child: Text('Meine Visitenkarte', style: TextStyle(color: Colors.black),)),
                                Tab (child: Text('Vorgesetzte', style: TextStyle(color: Colors.black),)),
             
                            ]
                        ),
                    ),
                        Expanded(
                            child: TabBarView(
                            children: [
                                ListView.builder(                
                                itemCount: 1, 
                                itemBuilder: (context, index) {
                                return Align(
                                alignment: Alignment.topCenter,
                                child : Container(   
                                    padding: new EdgeInsets.all(20.0),  
                                    child: Card(  
                                        shape: RoundedRectangleBorder(  
                                            borderRadius: BorderRadius.circular(0.0),  
                                        ),  
                                    color: Colors.white,  
                                    elevation: 10,  
                                    child: Column(
                                        children: <Widget>[  
                                            Padding(
                                                padding: EdgeInsets.only(top: 12),
                                                child: Align(alignment: Alignment.topLeft,
                                                    child: Text('Visitenkarte',
                                                        style: GoogleFonts.allertaStencil(fontSize: 22)
                                                    )
                                                ),
                                            ),
                                            ListTile( 
                                                leading: Image.asset('assets/ava.jpg'),
                                                title: Text('Greg Neu',
                                                    style: GoogleFonts.allertaStencil(fontSize:16)),
                                                    subtitle: Text.rich(
                                                TextSpan(
                                                    text: 'greg.neu@f-bootcamp.com',
                                                    style:GoogleFonts.mulish(
                                                    decoration: TextDecoration.underline,
                                                    ),
                                                     children: <TextSpan>[
                                                TextSpan(
                                                    text: ' \n\n Monteur',
                                                    style:GoogleFonts.mulish(),
                                                ),
                                            ], 
                                        ),
                 
                                    ),
              
                                ),
               
                            Image.asset('assets/qrcode 1.jpg'),
                            Align(alignment: Alignment.topLeft,
                                child: Text('Adresse',
                                style: GoogleFonts.allertaStencil(fontSize: 22))
                                ),
                            ListTile(  
                                leading:Image.asset('assets/adresse.jpg'),
                                title: Text('Flutter Bootcamp', style: GoogleFonts.allertaStencil()),
                                subtitle: Text.rich(
                            TextSpan(
                                text: '6783 Ayala Ave',
                                style:GoogleFonts.mulish(),
                                children: <TextSpan>[
                            TextSpan(
                                text: ' \n1200 Metro Manila',
                                    style:GoogleFonts.mulish(),
                            ),
                        ], 
                    ),
                    ),
                    
                ),
                    Align(alignment: Alignment.topLeft,
                    child: Text('Kontakt',style: GoogleFonts.allertaStencil(fontSize: 22))
                    ),
                    ListTile(
                    leading:Image.asset('assets/user contact.jpg'),
                  title:  Text.rich(
                  TextSpan(
                    text: 'T: +49 1234 56 789 01',
                    style:GoogleFonts.allertaStencil(),
                  children: <TextSpan>[
                   TextSpan(
                    text: ' \n F: +49 1234 56 789 01-2',
                    style:GoogleFonts.allertaStencil(),
                    ),
                    TextSpan(
                    text: '\n M: +49 1234 56',
                    style:GoogleFonts.allertaStencil(),
                    ),
                    TextSpan(
                    text: '\n E: greg.neuf@f-bootcamp.com',
                    style:GoogleFonts.allertaStencil(),
                    ),
                ], 
                 ),
                 
                 ),
                 subtitle: Text('www.flutter-bootcamp.com', 
                 style:GoogleFonts.mulish(color: Colors.lightBlue),),
                 
                
                    
                ),
                ListView.builder(
                itemCount: 1,
                itemBuilder: (context, index) {
                return Align(
                    alignment: Alignment.topCenter,
                child : Container(  
                    
                padding: new EdgeInsets.all(5.0),  
                 child: Card(  
                shape: RoundedRectangleBorder(  
                borderRadius: BorderRadius.circular(0.0),  
                ),  
                color: Colors.white,  
                elevation: 10,  
                child: Column(  
         
                children: <Widget>[
                 Align(alignment: Alignment.topLeft,
                child: Text('Visitenkarte',style: GoogleFonts.allertaStencil(fontSize: 22))
                ),  
               ListTile( 
                    leading: Image.asset('assets/ava.jpg'),
                  title: Text('Andero Mustermann', style: GoogleFonts.allertaStencil(fontSize:16)),
                  subtitle: Text.rich(
                  TextSpan(
                    text: 'andero.mustermann@f-bootcamp.com',
                    style:GoogleFonts.mulish(
                    decoration: TextDecoration.underline,
                    ),
                  children: <TextSpan>[
                   TextSpan(
                    text: ' \n\n Abteilungsleiter',
                    style:GoogleFonts.mulish(),
                    ),
                ], 
                 ),
                 
                 ),
              
                 ),
               
                Image.asset('assets/qrcode 1.jpg'),
                Align(alignment: Alignment.topLeft,
                child: Text('Adresse',style: GoogleFonts.allertaStencil(fontSize: 22))
                ),  
                  ListTile(  
                
                  leading:Image.asset('assets/adresse.jpg'),
                  title: Text('Flutter Bootcamp', style: GoogleFonts.allertaStencil()),
                   subtitle: Text.rich(
                  TextSpan(
                    text: '6783 Ayala Ave',
                    style:GoogleFonts.mulish(),
                  children: <TextSpan>[
                   TextSpan(
                    text: ' \n1200 Metro Manila',
                    style:GoogleFonts.mulish(),
                    ),
                ], 
                 ),
                 
                 ),
                    
                ),
                Align(alignment: Alignment.topLeft,
                child: Text('Kontakt',style: GoogleFonts.allertaStencil(fontSize: 22))
                ),  
                            ListTile(
                                    leading:Image.asset('assets/user contact.jpg'),
                                title:  Text.rich(
                                TextSpan(
                    text: 'T: +49 1234 56 789 01',
                    style:GoogleFonts.allertaStencil(),
                  children: <TextSpan>[
                   TextSpan(
                    text: ' \n F: +49 1234 56 789 01-2',
                    style:GoogleFonts.allertaStencil(),
                    ),
                    TextSpan(
                    text: '\n M: +49 1234 56',
                    style:GoogleFonts.allertaStencil(),
                    ),
                    TextSpan(
                    text: '\n E: andreo.mustermann@f-bo...',
                    style:GoogleFonts.allertaStencil(),
                    ),
                ], 
                 ),
                 
                 ),
                 subtitle: Text('www.flutter-bootcamp.com', 
                 style:GoogleFonts.mulish(color: Colors.lightBlue),),
                 
                
                    
                             ),
                        ],
                ),
                 ),
                 ),
                );
                        }
                        )
                ],
                ),
                    ),
                    ),
                );
                }
                ),

                ]
            
                ),
                        ),
                ] 
            ),
        ); 
    
    }
}