import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import '../widgets/app_drawer.dart';

class Homepage extends StatelessWidget{          
  @override
  Widget build(BuildContext context) {
      return Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.white,
             iconTheme: IconThemeData(color: Colors.black),
             
          ),
        drawer: AppDrawer(),
        
        body:Container( 
            width: double.infinity,
            padding: EdgeInsets.all(12),
            child: homepageForm, 
        ),
        backgroundColor: Colors.grey[200],
        
      );
      
  }
}

Widget mainCard = Card(
    
    child: Column(
        children: <Widget>[  
            Align(alignment: Alignment.topLeft,
                child:Text('Mein Konto',style: GoogleFonts.allertaStencil(fontSize: 22)),
            ),
       
            ListTile( 
                leading: Image.asset('assets/ava.jpg'),
                title: Text('Greg Neu', style: GoogleFonts.allertaStencil(fontSize:16)),
                subtitle: Text.rich(
                    TextSpan(
                        text: 'max.muster',
                        style:GoogleFonts.mulish(),
                        children: <TextSpan>[
                            TextSpan(
                                text: 'mann@bkl.de',
                                style:GoogleFonts.mulish(
                                    decoration: TextDecoration.underline,
                                ),
                            ),

                            TextSpan(
                                text: ' \n\n Monteur',
                                style:GoogleFonts.mulish(),
                            ),
                        ],            
                    ),
            
                )
            ),
            Align(alignment: Alignment.topLeft,
                child:Text('Ansprechpartner', style: GoogleFonts.allertaStencil(fontSize: 22)),
            ),
            ListTile(  
                leading: Image.asset('assets/flamingo.jpg'),
                title: Text('Ingo Flamingo', style: GoogleFonts.allertaStencil()),
                subtitle: Text('ingo.flamingo@f-bootcamp.com', style:GoogleFonts.mulish()),        
                     
            ),
            Align(alignment: Alignment.topLeft,
                child: Text('Wochenbericht', style: GoogleFonts.allertaStencil(fontSize: 22)),
            ),
            ListTile(  
                leading: Icon(FontAwesomeIcons.calendarAlt,size: 30,),
                title: Text('12.03-19.03.2021', style: GoogleFonts.allertaStencil()),
            ),
             ElevatedButton.icon(
                 label: Text('Krankheit einreichen',  style: GoogleFonts.mulish(color: Colors.white),), 
                 icon: Icon(FontAwesomeIcons.plus,color: Colors.white),

                 onPressed: () {  },

                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    primary: Colors.black,
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0)),
    
                ),
            ),
            Align(alignment: Alignment.topLeft,
                child: Text('Monatsbericht', style: GoogleFonts.allertaStencil(fontSize: 22)),
            ),
            ListTile(  
                leading: Icon(FontAwesomeIcons.calendarAlt,size: 30,),
                title: Text('April 2020', style: GoogleFonts.allertaStencil()),
            ),
             ElevatedButton.icon(
                 label: Text('Monatsbericht erstellen',  style: GoogleFonts.mulish(color: Colors.white),), 
                 icon: Icon(FontAwesomeIcons.solidPaperPlane,color: Colors.white),

                 onPressed: () {  },

                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            primary: Colors.black,
                            shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0)),
    
                        ),
             )
            
        ],
    ), 
);

Widget dateCard = Card( 
    child: Column(
        children: <Widget>[  
            Align(alignment: Alignment.topLeft,
                child:Text('Mein Konto',style: GoogleFonts.allertaStencil(fontSize: 22)),
            ),
       
            ListTile(  
                leading:Text('Urlaubstage',style: GoogleFonts.allertaStencil(fontSize: 14)) ,
                trailing:Text('25',style: GoogleFonts.allertaStencil(fontSize: 14))  ,
            ),
            ListTile(  
                leading:Text('Resturlaub EPOS',style: GoogleFonts.allertaStencil(fontSize: 14)) ,
                trailing:Text('10',style: GoogleFonts.allertaStencil(fontSize: 14))  ,
            ),
            ListTile(  
                leading:Text('Beantragt',style: GoogleFonts.allertaStencil(fontSize: 14)) ,
                trailing:Text('08',style: GoogleFonts.allertaStencil(fontSize: 14))  ,
            ),
            ListTile(  
                leading:Text('Übertrag Vorjahr',style: GoogleFonts.allertaStencil(fontSize: 14)), 
                subtitle: Text('(gültig bis 31.03.2021)',style: GoogleFonts.allertaStencil(fontSize: 16)),
                trailing:Text('01',style: GoogleFonts.allertaStencil(fontSize: 14))  ,
                isThreeLine: true,
            ),
            Container(
                    alignment: Alignment.bottomCenter,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                            Text('Impressum'),Center(
                                child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                    Icon(
                                        Icons.circle,
                                        color: Colors.orange,
                                        size: 30.0,
                                     ),
                                    Text("7",
                                        style: TextStyle(fontSize: 18,color: Colors.white),
                                    ),
                                ],
                            ),
                        ),
                    ]
                )
            ),
        ]
    ),
);

Widget sickCard = Padding( 
        padding: EdgeInsets.only(top: 12),
        child: Card( 
            child: Column(
                children: <Widget>[  
                Align(
                    alignment: Alignment.topLeft,
                    child:Text('Krankheistage',style: GoogleFonts.allertaStencil(fontSize: 22)),
                 ),
                ListTile(  
                    leading:Text('Insgesamt') ,
                    trailing:Text('03',style: GoogleFonts.allertaStencil(fontSize: 22)),
                ),
             
                   ElevatedButton.icon(
                 label: Text('Krankheit einreichen',  style: GoogleFonts.mulish(color: Colors.white),), 
                 icon: Icon(FontAwesomeIcons.plus,color: Colors.white),

                 onPressed: () {  },

                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            primary: Colors.black,
                            shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0)),
    
                        ),
             )
                  
            ],
               
        )        
    ),
);

Widget azKonto = Card( 
    
            child: Column(
                children: <Widget>[  
                Align(
                    alignment: Alignment.topLeft,
                    child:Text('AZ Konto',style: GoogleFonts.allertaStencil(fontSize: 22)),
                 ),
                ListTile(  
                    leading:Text('Stunden',style: GoogleFonts.mulish()) ,
                    trailing:Text('100/250',style: GoogleFonts.allertaStencil(fontSize: 22))  ,
                ),
            ],
        )        
    );

Widget homepageForm = SingleChildScrollView( 
    child: Form(
        child: Column(
            children: [
                mainCard,
                dateCard,
                sickCard,
                azKonto
            ]
        )
    )
);

       
    
    