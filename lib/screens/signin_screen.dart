import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

import '../screens/homepage_screen.dart';

class SigninScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _SigninScreen (); 
}

class _SigninScreen extends State<SigninScreen> {
  @override
  Widget build(BuildContext context) {
      
  
        Widget imageLogo = Container(
           
                margin: EdgeInsets.only(top: 163),
                child: Align(
                    child: Image.asset('assets/flutterlogo.jpg')
                )
            
        );

        Widget bodyText = Container(
           margin: EdgeInsets.only(top: 48),
           child: Text('Flutter FieldPass', 
            style: GoogleFonts.roboto( 
                textStyle:TextStyle(
                    fontWeight: FontWeight.bold,
                ),
            ),
          )
        );

        Widget btnSignin = Container(
            
          margin: EdgeInsets.only(top: 90),
          width: 302,
          height: 57,
            child: TextButton(
                child: Row(
                      children: <Widget>[
                        Image.asset(
                          'assets/microsoftlogo.jpg',
                           width: 50,
                        ),
                        Text('Sign in With Microsoft',
                            style: GoogleFonts.mulish(color: Colors.black,fontWeight: FontWeight.bold),
                        ),

                        Icon(FontAwesomeIcons.angleDoubleRight, size: 21,)
                      ],
                   ),

                    onPressed: () { 
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Homepage()),
                    );
         
                },
                    style: TextButton.styleFrom(
                        primary: Colors.black,
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                    ),
                    
            ),
                    decoration: BoxDecoration(
                    boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade400,
                        offset: Offset(0.0, 4.90)
        
                    )
                ]
             ),
        );
        Widget bottomText = Container(
            padding: EdgeInsets.only(top: 150.0),
             alignment: FractionalOffset.center,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: 
                [Text('Impressum'), Text('Datenschutz') ]
             ),
        );
      

        Widget signinForm = SingleChildScrollView( 
            child: Form(
                child: Column(
                    children: [
                        imageLogo,
                        bodyText,
                        btnSignin,
                        bottomText
                    ]
                )
            )
        );

        return Scaffold(
             body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(12.0),
                child: signinForm, color: Colors.white,
            )
        );

        
    }
}